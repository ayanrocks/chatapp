const express = require('express');
const http = require('http');
const app = express();
const server = http.createServer(app);
const io = require('socket.io').listen(server);

server.listen(5000, null, function() {
	console.log('Listening on port ' + 5000);
});

app.get('/', function(req, res) {
	res.sendFile(__dirname + '/client/build/index.html');
});

var rooms = {};
var sockets = {};

io.on('connection', socket => {
	socket.rooms = {};
	sockets[socket.id] = socket;

	console.log('[' + socket.id + '] connection accepted');
	socket.on('disconnect', function() {
		for (var room in socket.rooms) {
			part(room);
		}
		console.log('[' + socket.id + '] disconnected');
		delete sockets[socket.id];
	});

	function part(room) {
		console.log('[' + socket.id + '] part ');

		if (!(room in socket.rooms)) {
			console.log('[' + socket.id + '] ERROR: not in ', room);
			return;
		}

		delete socket.rooms[room];
		delete rooms[room][socket.id];

		for (id in rooms[room]) {
			rooms[room][id].emit('removePeer', { peer_id: socket.id });
			socket.emit('removePeer', { peer_id: id });
		}
	}
	socket.on('part', part);

	socket.on('exchange', function(data) {
		socket.broadcast.emit('exchange', data);
	});

	socket.on('join', data => {
		let room = data.room;
		let user = data.user;

		if (room in socket.rooms) {
			console.log('[' + socket.id + '] ERROR: already joined ', channel);
			return;
		}

		if (!(room in rooms)) {
			rooms[room] = {};
		}

		for (id in rooms[room]) {
			rooms[room][id].emit('addPeer', { peer_id: socket.id, should_create_offer: false });
			socket.emit('addPeer', { peer_id: id, should_create_offer: true });
		}

		rooms[room][socket.id] = socket;
		socket.rooms[room] = room;
	});

	socket.on('groupExchange', data => {
		if (data.from in sockets) {
			sockets[data.from].emit('groupExchange');
		}
	});
});
