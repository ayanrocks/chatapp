import React from 'react';
// import { webrtcHelper } from './helpers/webrtchelper';
import './Chat.css';
import io from 'socket.io-client';

class Chat extends React.Component {
	constructor() {
		super();
		this.state = {
			socket: null,
			msg: '',
			msgs: [],
			peerconn: null,
			dataChannel: null,
			modal: true,
		};
	}
	componentDidMount() {
		const configuration = {
			iceServers: [{ url: 'stun:stun.l.google.com:19302' }],
		};
		this.setState(
			{ socket: io('http://localhost:5000'), peerconn: new RTCPeerConnection(configuration) },
			this.init
		);
	}

	logError = error => {
		console.error(error);
	};

	init = () => {
		const { socket, peerconn } = this.state;
		socket.on('connect', () => {
			console.log('%c' + socket.id, 'background: green; color: #fff');
		});

		peerconn.onicecandidate = function(event) {
			console.log('onicecandidate');
			if (event.candidate) {
				const candidate = { from: socket.id, candidate: event.candidate, type: 'candidate' };
				socket.emit('exchange', candidate);
			}
		};
		peerconn.ondatachannel = event => {
			console.log('ondatachannel', event);
			const receiveChannel = event.channel;
			receiveChannel.onmessage = event => {
				const { msgs } = this.state;
				console.log(JSON.parse(event.data));

				msgs.push(JSON.parse(event.data));
				this.setState({ msgs });
			};
		};

		this.setState(
			{
				dataChannel: peerconn.createDataChannel('text_chan', {
					reliable: false,
				}),
			},
			this.dataChannelListener
		);
		socket.on('exchange', data => {
			this.exchange(data);
		});
	};

	dataChannelListener = () => {
		const { dataChannel } = this.state;

		dataChannel.onopen = e => {
			this.setState({ modal: false });
			console.log('%cdataChannel.onopen  ' + e.type, 'background: #187042; color: #fff');
		};
		dataChannel.onclose = () => {
			this.setState({ modal: true });

			console.log('dataChannel.onclose');
		};
		dataChannel.onmessage = function(event) {
			console.log('dataChannel.onmessage:', event.data);
		};
		dataChannel.onerror = function(error) {
			console.log('dataChannel.onerror', error);
		};
	};

	createOffer = () => {
		const { peerconn, socket } = this.state;
		peerconn
			.createOffer()
			.then(async offer => {
				console.log('createOffer', offer);
				await peerconn.setLocalDescription(new RTCSessionDescription(offer));
				let sdp = {
					from: socket.id,
					sdp: peerconn.localDescription,
					type: 'offer',
				};

				socket.emit('exchange', sdp);
			})
			.catch(this.logError);
	};

	exchange = data => {
		const { peerconn, socket } = this.state;
		if (data.type === 'offer' && data.sdp) {
			console.log('got offer', data);
			peerconn
				.setRemoteDescription(new RTCSessionDescription(data.sdp))
				.then(() => {
					console.log('CreateRemote Description');

					peerconn
						.createAnswer()
						.then(desc => {
							console.log('createAnswer', desc);
							peerconn
								.setLocalDescription(desc)
								.then(() => {
									console.log('setLocalDescription', peerconn.localDescription);
									socket.emit('exchange', {
										from: socket.id,
										sdp: peerconn.localDescription,
										type: 'answer',
									});
								})
								.catch(this.logError);
						})
						.catch(this.logError);
				})
				.catch(this.logError);
		}
		if (data.type === 'answer' && data.sdp) {
			console.log('Got Answer', data);

			peerconn.setRemoteDescription(new RTCSessionDescription(data.sdp));
		}
		if (data.type === 'candidate' && data.candidate) {
			if (peerconn.remoteDescription) {
				console.log('exchange candidate', data);
				peerconn.addIceCandidate(new RTCIceCandidate(data.candidate)).catch(this.logError);
			}
		}
	};

	onSendMsg = e => {
		e.preventDefault();
		const { dataChannel, socket, msg, msgs } = this.state;

		if (dataChannel.readyState === 'open' && msg.length > 0) {
			const msgDetails = { from: socket.id, msg };
			msgs.push(msgDetails);
			this.setState({ msgs, msg: '' });
			dataChannel.send(JSON.stringify(msgDetails));
		}
	};
	handleConnect = e => {
		e.preventDefault();
		// const { socket } = this.state;
		// webrtcHelper(socket);
		this.createOffer();
	};
	render() {
		return (
			<div className="Chat">
				{this.state.modal && (
					<div className="connect_modal">
						<div className="connect_modal__container">
							<p className="connectText">Connect to the other peer</p>
							<button className="connectButton" onClick={this.handleConnect}>
								Connect
							</button>
						</div>
					</div>
				)}
				<div id="messages">
					{this.state.msgs.length > 0 &&
						this.state.msgs.map((msg, i) => (
							<div className="msgBox" key={i}>
								<div className={this.state.socket.id === msg.from ? 'sender' : 'reciever'}>
									<span>{msg.msg}</span>
								</div>
							</div>
						))}
				</div>
				<form action="">
					<input
						id="m"
						autoComplete="off"
						value={this.state.msg}
						onChange={e => this.setState({ msg: e.target.value })}
					/>
					<button onClick={this.onSendMsg}>Send</button>
				</form>
			</div>
		);
	}
}

export default Chat;
