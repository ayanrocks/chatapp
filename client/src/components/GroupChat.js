import React from 'react';
// import { webrtcHelper } from './helpers/webrtchelper';
// import './GroupChat.css';
import io from 'socket.io-client';

class GroupChat extends React.Component {
	constructor(props) {
		super(props);
		this.state = {
			socket: null,
			msg: '',
			msgs: [],
			peers: {},
			peerconn: null,
			dataChannel: null,
			modal: true,
		};
	}
	componentDidMount() {
		const configuration = {
			iceServers: [{ url: 'stun:stun.l.google.com:19302' }],
		};
		this.setState(
			{ socket: io('http://localhost:5000'), peerconn: new RTCPeerConnection(configuration) },
			this.init
		);
	}

	logError = error => {
		console.error(error);
	};

	init = () => {
		const { socket, peerconn, peers } = this.state;
		socket.on('connect', () => {
			console.log('%c' + socket.id, 'background: green; color: #fff');
		});

		socket.on('disconnect', () => {
			console.log('Disconnected from signaling server');
			/* Tear down all of our peer connections and remove all the
			 * media divs when we disconnect */
			for (socket.id in peers) {
				peers[socket.id].close();
			}
			this.setState({ peers: {} });
		});

		socket.emit('join', { room: '1', user: socket.id });

		socket.on('addPeer', data => {
			let peer_id = data.peer_id;
			if (peer_id in peers) {
				console.log('Already connected to peer ', peer_id);
				return;
			}

			peers[peer_id] = peerconn;

			if (data.should_create_offer) {
				console.log('Creating RTC offer to ', peer_id);
				this.createOffer();
			}
		});

		socket.on('removePeer', data => {
			console.log('Signaling server said to remove peer:', data);
			var peer_id = data.peer_id;
			if (peer_id in peers) {
				peers[peer_id].close();
			}
			delete peers[peer_id];
		});

		peerconn.onicecandidate = event => {
			console.log('onicecandidate');
			if (event.candidate) {
				const candidate = {
					from: socket.id,
					roomName: this.props.match.params.roomName,
					candidate: event.candidate,
					type: 'candidate',
				};
				socket.emit('groupExchange', candidate);
			}
		};
		peerconn.ondatachannel = event => {
			console.log('ondatachannel', event);
			const receiveChannel = event.channel;
			receiveChannel.onmessage = event => {
				const { msgs } = this.state;
				console.log(JSON.parse(event.data));

				msgs.push(JSON.parse(event.data));
				this.setState({ msgs });
			};
		};

		this.setState(
			{
				dataChannel: peerconn.createDataChannel('text_chan', {
					reliable: false,
				}),
			},
			this.dataChannelListener
		);

		socket.on('group exchange', data => {
			this.exchange(data);
		});
	};

	dataChannelListener = () => {
		const { dataChannel } = this.state;

		dataChannel.onopen = e => {
			this.setState({ modal: false });
			console.log('%cdataChannel.onopen  ' + e.type, 'background: #187042; color: #fff');
		};
		dataChannel.onclose = () => {
			this.setState({ modal: true });

			console.log('dataChannel.onclose');
		};
		dataChannel.onmessage = function(event) {
			console.log('dataChannel.onmessage:', event.data);
		};
		dataChannel.onerror = function(error) {
			console.log('dataChannel.onerror', error);
		};
	};

	createOffer = () => {
		const { peerconn, socket } = this.state;
		peerconn
			.createOffer()
			.then(async offer => {
				console.log('createOffer', offer);
				await peerconn.setLocalDescription(new RTCSessionDescription(offer));
				let sdp = {
					from: socket.id,
					roomName: this.props.match.params.roomName,
					sdp: peerconn.localDescription,
					type: 'offer',
				};

				socket.emit('exchange', sdp);
			})
			.catch(this.logError);
	};

	exchange = data => {
		const { socket, peers } = this.state;
		if (data.type === 'offer' && data.sdp) {
			console.log('got offer', data);
			var peer_id = data.from;
			var peer = peers[peer_id];
			peer.setRemoteDescription(new RTCSessionDescription(data.sdp))
				.then(() => {
					console.log('CreateRemote Description');

					peer.createAnswer()
						.then(desc => {
							console.log('createAnswer', desc);
							peer.setLocalDescription(desc)
								.then(() => {
									console.log('setLocalDescription', peer.localDescription);
									socket.emit('exchange', {
										from: socket.id,
										roomName: this.props.match.params.roomName,
										sdp: peer.localDescription,
										type: 'answer',
									});
								})
								.catch(this.logError);
						})
						.catch(this.logError);
				})
				.catch(this.logError);
		}
		if (data.type === 'answer' && data.sdp) {
			console.log('Got Answer', data);

			peer.setRemoteDescription(new RTCSessionDescription(data.sdp));
		}
		if (data.type === 'candidate' && data.candidate) {
			if (peer.remoteDescription) {
				console.log('exchange candidate', data);
				peer.addIceCandidate(new RTCIceCandidate(data.candidate)).catch(this.logError);
			}
		}
	};

	onSendMsg = e => {
		e.preventDefault();
		const { dataChannel, socket, msg, msgs } = this.state;

		if (dataChannel.readyState === 'open' && msg.length > 0) {
			const msgDetails = { from: socket.id, msg };
			msgs.push(msgDetails);
			this.setState({ msgs, msg: '' });
			dataChannel.send(JSON.stringify(msgDetails));
		}
	};
	handleConnect = e => {
		e.preventDefault();
		// const { socket } = this.state;
		// webrtcHelper(socket);
		this.createOffer();
	};
	render() {
		return (
			<div className="GroupChat">
				{/* {this.state.modal && (
					<div className="connect_modal">
						<div className="connect_modal__container">
							<p className="connectText">Connect to the other peer</p>
							<button className="connectButton" onClick={this.handleConnect}>
								Connect
							</button>
						</div>
					</div>
				)} */}
				<div id="messages">
					{this.state.msgs.length > 0 &&
						this.state.msgs.map((msg, i) => (
							<div className="msgBox" key={i}>
								<div className={this.state.socket.id === msg.from ? 'sender' : 'reciever'}>
									<span>{msg.msg}</span>
								</div>
							</div>
						))}
				</div>
				<form action="">
					<input
						id="m"
						autoComplete="off"
						value={this.state.msg}
						onChange={e => this.setState({ msg: e.target.value })}
					/>
					<button onClick={this.onSendMsg}>Send</button>
				</form>
			</div>
		);
	}
}

export default GroupChat;
