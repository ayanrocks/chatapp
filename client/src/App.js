import React from 'react';
// import { webrtcHelper } from './helpers/webrtchelper';
import './App.css';
import { BrowserRouter, Switch, Route, Link } from 'react-router-dom';
import Chat from './components/Chat';
import GroupChat from './components/GroupChat';

class App extends React.Component {
	constructor() {
		super();
		this.state = {
			modal: true,
		};
	}
	render() {
		return (
			<div className="App">
				<BrowserRouter>
					<Link to="/">
						<h1>Welcome to Chat App</h1>
					</Link>
					<Link to="/chat" className="links">
						Go to Chat
					</Link>
					<Link to="/groupchat/1" className="links">
						Go to GroupChat room 1
					</Link>
					<Switch>
						{/* <Route exact path="/" component={App} /> */}
						<Route exact path="/chat" component={Chat} />
						<Route exact path="/groupchat/:roomName" component={GroupChat} />
					</Switch>
				</BrowserRouter>
			</div>
		);
	}
}

export default App;
