//! DEPRECATED. LOGIC MOVED TO COMPONENT FILE. JUST FOR REFERENCE

function logError(error) {
	console.error(error);
}

export const webrtcHelper = socket => {
	const configuration = {
		iceServers: [{ url: 'stun:stun.l.google.com:19302' }],
	};

	//!DONT USE
	const connection = {
		optional: [{ DtlsSrtpKeyAgreement: true }, { RtpDataChannels: true }],
	};
	const peerconn = new RTCPeerConnection(configuration, connection);

	//!DONT USE
	const sdpConstraints = {
		mandatory: { OfferToReceiveAudio: false, OfferToReceiveVideo: false },
	};

	const dataChannel = peerconn.createDataChannel('text_chan', {
		reliable: true,
	});

	peerconn.textDataChannel = dataChannel;

	peerconn.onnegotiationneeded = async function(event) {
		console.log('onnegotiationneeded');

	};

	peerconn.onicecandidate = function(event) {
		console.log('onicecandidate');
		const candidate = { from: socket.id, candidate: event.candidate, type: 'candidate' };
		socket.emit('exchange', candidate);
	};
	peerconn.ondatachannel = function(event) {
		console.log('ondatachannel');
		const receiveChannel = event.channel;
		if (!peerconn.textDataChannel) {
			peerconn.textDataChannel = receiveChannel;
		}
	};

	peerconn.oniceconnectionstatechange = function(event) {
		console.log('oniceconnectionstatechange', event);
	};
	peerconn.onsignalingstatechange = function(e) {
		console.log('onsignalingstatechange', e);
	};
	peerconn.onaddstream = function() {
		console.log('onaddstream');
	};
	peerconn.onremovestream = function() {
		console.log('onremovestream');
	};

	dataChannel.onopen = function() {
		console.log('dataChannel.onopen');
	};
	dataChannel.onclose = function() {
		console.log('dataChannel.onclose');
	};
	dataChannel.onmessage = function(event) {
		console.log('dataChannel.onmessage:', event.data);
	};
	dataChannel.onerror = function(error) {
		console.log('dataChannel.onerror', error);
	};

	peerconn
		.createOffer(sdpConstraints)
		.then(offer => {
			console.log('createOffer', offer);
			peerconn.setLocalDescription(new RTCSessionDescription(offer));
			let sdp = {
				from: socket.id,
				sdp: peerconn.localDescription,
				type: 'offer',
			};
	
			socket.emit('exchange', sdp);
		})
		.catch(logError);

	socket.on('exchange', data => {
		exchange(data, peerconn, socket);
	});
};

function exchange(data, peerconn, socket) {
	if (data.type === 'offer' && data.sdp) {
		console.log('got offer', data);
		peerconn
			.setRemoteDescription(new RTCSessionDescription(data.sdp))
			.then(() => {
				console.log('CreateRemote Description');

				peerconn
					.createAnswer()
					.then(desc => {
						console.log('createAnswer', desc);
						peerconn
							.setLocalDescription(desc)
							.then(desc => {
								console.log('setLocalDescription', peerconn.localDescription);
								socket.emit('exchange', {
									from: data.from,
									sdp: peerconn.localDescription,
									type: 'answer',
								});
							})
							.catch(logError);
					})
					.catch(logError);
			})
			.catch(logError);
	}
	if (data.type === 'answer' && data.sdp) {
		console.log("Got Answer", data);
		
		peerconn.setRemoteDescription(new RTCSessionDescription(data.sdp));
	}
	if (data.type === 'candidate' && data.candidate) {
		console.log('exchange candidate', data);
		if (peerconn.remoteDescription) {
			peerconn
				.addIceCandidate(new RTCIceCandidate(data.candidate))
				.then(() => {
					peerconn.textDataChannel.send('CONNECTED');
				})
				.catch(logError);
		}
	}
}
